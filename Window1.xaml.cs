﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using Microsoft.Win32;
using System.IO;
using IOPath = System.IO.Path;
using System.Text.RegularExpressions;
using System.Globalization;

using System.Web;
using System.Net;

namespace SpiderGmaps {
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window {
		private delegate void EmptyDelegate();
		string[] linhasPlan;
		FileInfo nomeCsv = null;
		string urlGmapsRotas = "http://maps.google.com/m/directions?saddr={0}&daddr={1}&hl=pt-BR&oi=nojs";
		Dictionary<string,string> listaEstados;
		public Window1() {
			InitializeComponent();

			prepIU("");

			listaEstados = new Dictionary<string, string>();
			listaEstados.Add("AC", "Acre");
			listaEstados.Add("AL", "Alagoas");
			listaEstados.Add("AP", "Amapá");
			listaEstados.Add("AM", "Amazonas");
			listaEstados.Add("BA", "Bahia");
			listaEstados.Add("CE", "Ceará");
			listaEstados.Add("DF", "Distrito Federal");
			listaEstados.Add("ES", "Espírito Santo");
			listaEstados.Add("GO", "Goiás");
			listaEstados.Add("MA", "Maranhão");
			listaEstados.Add("MT", "Mato Grosso");
			listaEstados.Add("MS", "Mato Grosso do Sul");
			listaEstados.Add("MG", "Minas Gerais");
			listaEstados.Add("PA", "Pará");
			listaEstados.Add("PB", "Paraíba");
			listaEstados.Add("PR", "Paraná");
			listaEstados.Add("PE", "Pernambuco");
			listaEstados.Add("PI", "Piauí");
			listaEstados.Add("RJ", "Rio de Janeiro");
			listaEstados.Add("RN", "Rio Grande do Norte");
			listaEstados.Add("RS", "Rio Grande do Sul");
			listaEstados.Add("RO", "Rondônia");
			listaEstados.Add("RR", "Rorâima");
			listaEstados.Add("SC", "Santa Catarina");
			listaEstados.Add("SP", "São Paulo");
			listaEstados.Add("SE", "Sergipe");
			listaEstados.Add("TO", "Tocantins");
		}

		private void btAbreCSV_Click(object sender, RoutedEventArgs e) {
			bool? res;

			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "Arquivo texto com campos separado por vírgulas (.csv)|*.csv";
			dlg.DefaultExt = ".csv";

			#if !DEBUG
			res = dlg.ShowDialog();
			#else
			res = true;
			#endif

			if (res == true) {
				#if DEBUG
				dlg.FileName = "C:\\Users\\fernando\\Documents\\OrigemDestKm.csv";
				#endif

				FileInfo fi = new FileInfo(dlg.FileName);
				if (fi.Length > 2097152) {
					MessageBox.Show("Tamanho do arquivo muito grande. Tamanho máximo\npermitido é 2MBytes.", "Arquivo muito grande", MessageBoxButton.OK);
					prepIU("");
					return;
				}

				linhasPlan = File.ReadAllLines(dlg.FileName, Encoding.Default);

				if (linhasPlan[0].Split(';').Length != 4 || linhasPlan.Length < 2) {
					MessageBox.Show("O formato do arquivo selecionado parece inválido!\nVerifique o formato do arquivo (deve conter 4 colunas\ne no mínimo 2 linhas, sendo a primeira linha o cabeçalho)", "Erro de formato do arquivo", MessageBoxButton.OK, MessageBoxImage.Error);
					prepIU("");
					return;
				}

				prepIU(dlg.FileName);
			}
		}

		private bool prepIU(string fn) {
			btIniciaProc.IsEnabled = !string.IsNullOrEmpty(fn);
			nomeArq.Content = fn;
			nomeCsv = (btIniciaProc.IsEnabled ? new FileInfo(fn) : null);
			logBox1.Content = (btIniciaProc.IsEnabled ? "Pronto para iniciar. Clique no botão acima..." : "Aguardando seleção do arquivo...");
			return btIniciaProc.IsEnabled;
		}

		private string TitleCase(string str) {
			return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str);
		}

		private void btIniciaProc_Click(object sender, RoutedEventArgs e) {
			string[] campos;
			string qryOrigem, qryDestino, urlFinal;
			string divCidadeDestRegex = "<div class=\"[0-9a-z]+\">{0} - .+?</div>";
			string spanDistanciaRegex = "<span class=\"[0-9a-z]+\">([0-9.,]+) ?([a-z]+)</span>";
			List<string[]> novoCSV = new List<string[]>();
			novoCSV.Add(new string[] { "CIDADE_ORIG", "UF_ORIG", "CIDADE_DEST", "UF_DEST", "DIST", "UN_DIST" });

			pgb.Minimum = 1;
			pgb.Maximum = linhasPlan.Length;
			pgb.Value = 1;
			pgb.IsIndeterminate = false;

			for (int l = 1; l < linhasPlan.Length; l++) {
				campos = linhasPlan[l].Split(';');

				if (campos.Length >= 4) {
					for (int c = 0; c < 4; c += 2) campos[c] = TitleCase(campos[c]).Trim();
					for (int c = 1; c < 4; c += 2) campos[c] = campos[c].ToUpper().Trim();

					logBox1.Content = string.Format("{0} - {1} à {2} - {3}", campos[0], campos[1], campos[2], campos[3]); 

					try {
						logBox2.Content = "Montando querystring com cidades origem e destino...";
						DoEvents();
						campos = campos.Select(el => el.Trim('"')).ToArray();

						qryOrigem = HttpUtility.UrlEncode(campos[0] + " - " + GetEstadoExtenso(campos[1]) + ", Brasil");
						qryDestino = HttpUtility.UrlEncode(campos[2] + " - " + GetEstadoExtenso(campos[3]) + ", Brasil");

						urlFinal = string.Format(urlGmapsRotas, qryOrigem, qryDestino);
						pgb.Value += 0.25;

						logBox2.Content = "Fazendo request ao Google para a rota...";
						DoEvents();
						HttpWebRequest req = (HttpWebRequest)WebRequest.Create(urlFinal);
						req.Method = "GET";

						StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
						string pagina = stIn.ReadToEnd();
						stIn.Close();
						pgb.Value += 0.25;

						logBox2.Content = "Tratando retorno - Passo 1...";
						DoEvents();
						string body = Regex.Match(pagina, "<body.*</body>").Value;
						pgb.Value += 0.20;
						logBox2.Content = "Tratando retorno - Passo 2...";
						DoEvents();
						string linhaDist = Regex.Match(body, string.Format(divCidadeDestRegex, campos[2]), RegexOptions.IgnoreCase).Value;
						pgb.Value += 0.20;
						logBox2.Content = "Tratando retorno - Passo 3...";
						DoEvents();
						Match mFinal = Regex.Match(linhaDist, spanDistanciaRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase);
						pgb.Value += 0.10;

						if (mFinal.Success && mFinal.Groups.Count == 3) {
							novoCSV.Add(new string[] { campos[0], campos[1], campos[2], campos[3], mFinal.Groups[1].Value.Replace(".",""), mFinal.Groups[2].Value });
							logBox2.Content = "Dados obtidos com sucesso! Seguindo para o próximo...";
						} else {
							logBox2.Content = "Não foi possível obter os dados por algum motivo... Ignorando...";
						}
						DoEvents();
					} catch (Exception ex) {
						logBox2.Content = "Erro desconhecido ao processar linha atual... Ignorando...";
						pgb.Value = l + 1;
						DoEvents();
					}
				} else {
					logBox2.Content = "Linha incorretamente formatada! Ignorando...";
					pgb.Value += 1;
					DoEvents();
				}
			}

			logBox1.Content = "Processo concluído. Salvando CSV com os dados colhidos...";
			logBox2.Content = "";
			DoEvents();
			string novoArqCSV = IOPath.Combine(nomeCsv.DirectoryName, IOPath.GetFileNameWithoutExtension(nomeCsv.FullName) + "-distancias.csv");

			File.WriteAllLines(novoArqCSV, novoCSV.Select(l => string.Join(";", l)).ToArray(), Encoding.Default);
			logBox1.Content = "Fim!";
			pgb.IsIndeterminate = true;
		}

		private string GetEstadoExtenso(string uf) {
			return listaEstados.First(k => k.Key == uf).Value;
		}

		public static void DoEvents() {
			Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background, new EmptyDelegate(delegate { }));
		}
	}
}
